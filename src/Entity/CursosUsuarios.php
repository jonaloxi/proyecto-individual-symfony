<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CursosUsuarios
 *
 * @ORM\Table(name="cursos_usuarios", indexes={@ORM\Index(name="curso-id", columns={"id_curso"}), @ORM\Index(name="IDX_7B4DE1C7FCF8192D", columns={"id_usuario"})})
 * @ORM\Entity(repositoryClass="App\Repository\CursosUsuariosRepository")
 */
class CursosUsuarios
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date", nullable=false)
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $fecha;

    /**
     * @var float|null
     *
     * @ORM\Column(name="nota", type="float", precision=10, scale=0, nullable=true)
     */
    private $nota;

    /**
     * @var Cursos
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Cursos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_curso", referencedColumnName="id")
     * })
     */
    private $idCurso;

    /**
     * @var Usuarios
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuario", referencedColumnName="id")
     * })
     */
    private $idUsuario;

    /**
     * @param \DateTime $fecha
     * @param float|null $nota
     * @param Cursos $idCurso
     * @param Usuarios $idUsuario
     */
    public function __construct(\DateTime $fecha, ?float $nota, Cursos $idCurso, Usuarios $idUsuario)
    {
        $this->fecha = $fecha;
        $this->nota = $nota;
        $this->idCurso = $idCurso;
        $this->idUsuario = $idUsuario;
    }

    /**
     * @return \DateTime
     */
    public function getFecha(): \DateTime
    {
        return $this->fecha;
    }

    /**
     * @param \DateTime $fecha
     */
    public function setFecha(\DateTime $fecha): void
    {
        $this->fecha = $fecha;
    }

    /**
     * @return float|null
     */
    public function getNota(): ?float
    {
        return $this->nota;
    }

    /**
     * @param float|null $nota
     */
    public function setNota(?float $nota): void
    {
        $this->nota = $nota;
    }

    /**
     * @return Cursos
     */
    public function getIdCurso(): Cursos
    {
        return $this->idCurso;
    }

    /**
     * @param Cursos $idCurso
     */
    public function setIdCurso(Cursos $idCurso): void
    {
        $this->idCurso = $idCurso;
    }

    /**
     * @return Usuarios
     */
    public function getIdUsuario(): Usuarios
    {
        return $this->idUsuario;
    }

    /**
     * @param Usuarios $idUsuario
     */
    public function setIdUsuario(Usuarios $idUsuario): void
    {
        $this->idUsuario = $idUsuario;
    }


}
