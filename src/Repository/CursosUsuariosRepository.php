<?php

namespace App\Repository;

use App\Entity\CursosUsuarios;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use function Symfony\Component\DependencyInjection\Loader\Configurator\service;

class CursosUsuariosRepository extends ServiceEntityRepository
{
    private $em;
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CursosUsuarios::class);
        $this->em = $this->getEntityManager();
    }

    public function findAllUserByCourse($id):array
    {

        $query = $this->em->createQuery(
       'SELECT cu.nota, c.nombre, u.nombre, cu.fecha FROM App\Entity\CursosUsuarios cu
       INNER JOIN cu.idCurso c
       INNER JOIN cu.idUsuario u
       WHERE c.id = :id');

        $query->setParameter('id', $id);
        return $query->getResult();
    }

    public function findNoteByUser($idUsuario, $idCurso):array
    {
        $query = $this->em->createQuery(
        "SELECT cu.nota, u.nombre as nombre_usuario, c.nombre as nombre_curso FROM App\Entity\CursosUsuarios cu
        INNER JOIN cu.idCurso c
        INNER JOIN cu.idUsuario u
        WHERE cu.idUsuario = :id AND cu.idCurso = :idCurso");
        $query->setParameter('id', $idUsuario);
        $query->setParameter('idCurso', $idCurso);
        return $query->getResult();
    }

    public function findExistEntry($idUsuario, $idCurso): array {
        $query = $this->em->createQuery(
            "SELECT cu.nota FROM App\Entity\CursosUsuarios cu 
            WHERE cu.idUsuario = :idUsuario AND cu.idCurso = :idCurso"
        );
        $query->setParameter("idCurso", $idCurso);
        $query->setParameter("idUsuario", $idUsuario);
        return $query->getResult();
    }
}