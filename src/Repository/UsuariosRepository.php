<?php

namespace App\Repository;
use App\Entity\Usuarios;
use Doctrine\Persistence\ManagerRegistry;

class UsuariosRepository extends \Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository
{
    private $em;
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Usuarios::class);
        $this->em = $this->getEntityManager();
    }

    public function findUserByNameAndPassword($nombre, $password)
    {
        $query = $this->em->createQuery('SELECT u FROM App\Entity\Usuarios u WHERE u.nombre = :nombre AND u.password = :password');
        $query->setParameter('nombre', $nombre);
        $query->setParameter('password', $password);
        return $query->getResult();
    }

    public function findAllUsersWithoutPassword(){
        $query = $this->em->createQuery("SELECT u.nombre, u.id FROM App\Entity\Usuarios u");
        return $query->getResult();
    }
}