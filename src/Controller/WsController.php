<?php

namespace App\Controller;

use App\Entity\Cursos;
use App\Entity\CursosUsuarios;
use App\Entity\Usuarios;
use DateTime;
use PharIo\Version\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use \Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Date;

class WsController extends AbstractController
{
    /**
     * @Route("/ws", name="app_ws")
     */
    public function index(): Response
    {
        $parametros = array();
        $parametros['titulo'] = 'Web Services';

        return $this->render('ws/index.html.twig', $parametros);
    }

    /**
     * @Route("/ws/cursos", name="cursos", methods={"GET"})
     */
    public function cursos(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $cursos = $em->getRepository(Cursos::class)->findAll();
        return $this->json($cursos);
    }

    /**
     * @Route("/ws/usuarios", name="get_all_users", methods={"GET"})
     */
    public function usuarios(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $usuarios = $em->getRepository(Usuarios::class)->findAllUsersWithoutPassword();
        return $this->json($usuarios);
    }

    /**
     * @Route("/ws/usuario/login", name="login" , methods={"POST"})
     */
    public function login(Request $request): JsonResponse
    {
        $datos = json_decode($request->getContent());

        $nombre = $datos->username;
        $password = $datos->password;

        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository(Usuarios::class)->findUserByNameAndPassword($nombre, $password);
        if ($usuario) {
            return $this->json($usuario);
        } else {
            return $this->json(['error' => 'Usuario no encontrado']);
        }
    }

    /**
     * @Route("/ws/usuario/add", name="registro" , methods={"POST"})
     */
    public function registro(Request $request): JsonResponse
    {
        $nombre = $request->get('nombre');
        $apellido1 = $request->get('apellido1');
        $apellido2 = $request->get('apellido2');
        $password = $request->get('password');
        $correo = $request->get('correo');
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository(Usuarios::class)->findUserByNameAndPassword($nombre, $password);
        if ($usuario) {
            return $this->json(['error' => 'Usuario ya existe']);
        } else {
            $usuario = new Usuarios($nombre, $apellido1, $apellido2, $password, $correo);
            $em->persist($usuario);
            $em->flush();
            return $this->json(['success' => 'Usuario registrado']);
        }
    }

    /**
     * @Route("/ws/usuario/update", name="actualizar" , methods={"PUT"})
     */
    public function actualizar(Request $request): JsonResponse
    {
        $id = $request->get('id');
        $nombre = $request->get('nombre');
        $apellido1 = $request->get('apellido1');
        $apellido2 = $request->get('apellido2');
        $password = $request->get('password');
        $correo = $request->get('correo');
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository(Usuarios::class)->find($id);
        if ($usuario) {
            $usuario->setNombre($nombre);
            $usuario->setApellido1($apellido1);
            $usuario->setApellido2($apellido2);
            $usuario->setPassword($password);
            $usuario->setCorreo($correo);
            $em->persist($usuario);
            $em->flush();
            return $this->json(['success' => 'Usuario actualizado']);
        } else {
            return $this->json(['error' => 'Usuario no encontrado']);
        }
    }

    /**
     * @Route("/ws/curso/{id}", name="users_by_course" , methods={"GET"})
     */
    public function usersByCourse($id): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        $curso = $em->getRepository(CursosUsuarios::class)->findAllUserByCourse($id);
        if ($curso) {

            return $this->json($curso);
        } else {
            return $this->json(['error' => 'Curso no encontrado']);
        }
    }

    /**
     * @Route("/ws/curso/notas", name="get_notes_by_user" , methods={"POST"})
     */
    public function getNotesByUser(Request $request): JsonResponse
    {

        $datos = json_decode($request->getContent());

        $idUsuario = $datos->idUsuario;
        $idCurso = $datos->idCurso;
//        $idUsuario = $request->get("idUsuario");
//        $idCurso = $request->get("idCurso");

        $em = $this->getDoctrine()->getManager();
        $curso = $em->getRepository(CursosUsuarios::class)->findNoteByUser($idUsuario, $idCurso);
        if ($curso) {
            return $this->json($curso);
        } else {
            return $this->json(['error' => 'No Tienes notas en este curso']);
        }
    }

    /**
     * @Route("/ws/curso/alta", name="alta_alumno_curso" , methods={"POST"})
     */
    public function altaAlumnado(Request $request): JsonResponse {
        $datos = json_decode($request->getContent());
        $fecha = \date('Y-m-d', strtotime($datos->fecha)) ;
        $fecha = DateTime::createFromFormat("Y-m-d", $fecha);

        $em = $this->getDoctrine()->getManager();

        $idUsuario = $em->getRepository(Usuarios::class)->find($datos->idUsuario);
        $idCurso = $em->getRepository(Cursos::class)->find($datos->idCurso);
        $nota = $datos->nota;
        $existe = $em->getRepository(CursosUsuarios::class)->findExistEntry($datos->idUsuario, $datos->idCurso);
        if (!$existe) {
            $altaAlumno = new CursosUsuarios($fecha, $nota, $idCurso, $idUsuario);

            $em->persist($altaAlumno);
            $em->flush();
            return $this->json(["message" => "Dado de alta correctamente", "status" => 200]);
        }else {
            return $this->json(["message" => "El usuario ya se ha registrado", "status" => 404]);
        }
    }


    // NO se esta usando
    private function converto2Json($object):JsonResponse {
        $encoders = [new XmlEncoder(), new JsonEncoder()];

        $normalizers = [new DateTimeNormalizer(), new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        $normalize = $serializer->normalize($object, null , array(DateTimeNormalizer::FORMAT_KEY => 'Y/m/d'));

        $jsonContent = $serializer->serialize($normalize, 'json');

        return JsonResponse::fromJsonString($jsonContent, 200);
    }
}